export const apiUrl = 'https://r7051nukg0.execute-api.eu-central-1.amazonaws.com/v1/'
export const outlookApiUrl = 'https://outlook.office.com/api/v2.0/'
export const subscription = {
    "payload": {
        "@odata.type":"#Microsoft.OutlookServices.PushSubscription",
        "Resource": "https://outlook.office.com/api/v2.0/me/events?$select=Organizer,Location,Start,End,Attendees,Subject",
        "NotificationURL": "https://r7051nukg0.execute-api.eu-central-1.amazonaws.com/v1/notifications",
        "ChangeType": "Created, Updated, Deleted",
        "ClientState": "c75831bd-fad3-4191-9a66-280a48528679"
    }
}
