import React from 'react'

import './styles.css';

class DataField extends React.Component {
  render() {
    const { label, content, width } = this.props
    return (
      <div
        className="datafield"
        style={{ width }}
      >
        <div className="data-label">{label}</div>
        <div>{content}</div>
      </div>
    )
  }
}

export default DataField
