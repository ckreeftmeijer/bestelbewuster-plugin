import React from 'react'

import Icons from '../../../images/icons'

import './styles.css';

class StatusIcon extends React.Component {
  render() {
    const {status, extraText } = this.props
    const statusses = {
      // BB statusses
       REGISTERED: {
        translation: 'besteld',
        icon: Icons.ShoppingCart
       },
       PROCESSED: {
        translation: 'definitief',
        icon: Icons.Star
       },
       CANCELLED: {
        translation: 'afgelast',
        icon: Icons.Cancel
       },
       // Email statusses
       ACCEPTED: {
        translation: 'geaccepteerd',
        icon: Icons.Happy
       },
       NEEDS_ACTION: {
        translation: 'nog niet geantwoord',
        icon: Icons.Warning
       },
       DECLINED: {
        translation: 'geweigerd',
        icon: Icons.Unhappy
       },
       NOTRESPONDED: {
        translation: 'niet gereageerd',
        icon: Icons.Unhappy // Shouldnt this be NOLUNCH ?
       },
       TENTATIVE: {
        translation: 'misschien',
        icon: Icons.Maybe
       },
       CONFIRMED : {
        translation: 'aanwezig',
        icon: Icons.Happy // Is this the same as ACCFEPTED?
       },
       SENT: {
        translation: 'nog niet geantwoord',
        icon: Icons.Warning
       },
       // No status
       NONE: {
         translation: 'geen status',
         icon: Icons.Unhappy
       }
    }

    return (
      <div
        className="status-icon"
      >
        <img src={statusses[status].icon} alt="status icon" />
        <span className="title-case">{statusses[status].translation}</span>&nbsp;
        <span>{extraText}</span>
      </div>
    )
  }
}

export default StatusIcon
