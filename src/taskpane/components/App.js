import React from 'react'
import { Collapse } from 'react-collapse';
import moment from 'moment'
import * as md5 from 'js-md5';

import UserIcon from '../../images/icons/users.png'
import PlusIcon from '../../images/icons/plus.svg'
import EditIcon from '../../images/icons/edit.svg'

import Progress from './Progress';
import Header from './Header';
import DataField from './DataField';
import StatusIcon from './StatusIcon';

import BestelBewusterService from '../services/BestelBewuster'
import OfficeJSService from '../services/OfficeJS'
import OutlookService from '../services/Outlook'

import {
  showMessage,
  addFooterText,
  removeFooterText
} from '../utils'

const bestelbewuster = new BestelBewusterService()
const officeJsService = new OfficeJSService()
const outlook = new OutlookService()

export default class App extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      isAuthorized: true,
      meetingCreated: false,
      showSnack: false,
      event: undefined,
      openCollapse: undefined,
      isWithin48Hours: false,
      showFooterText: true, // Can we save this in the event?
    };
  }

  componentDidMount() {
    if (this.props.isOfficeInitialized) {

      officeJsService.getStartDate().then(date => {
        const within48hours = this.checkDateDiff(date) < 48
        if (within48hours) {
          this.setState({ isWithin48Hours: true })
        }
      })

      Office.context.mailbox.item.addHandlerAsync(Office.EventType.AppointmentTimeChanged,
          this.handleAppointmentTimeChange
      );


      this.initializeApp()
    }
    // Note: I made seperate booleans for these but I guess once we connect
    // the BE we can derive from this data what the state of the app should be

    // TODO: Check if the user is authorized and if so set isAuthorized to true
    // TODO: Check if meeting time is within 24 hours and if so set isWithin48Hours to true
    // TODO: Set the meeting reponse object in the state
  }

  componentDidUpdate(prevProps, prevState) {
    if (!this.state.showFooterText && prevState.showFooterText) {
      removeFooterText()
    }
    if (this.state.showFooterText && !prevState.showFooterText) {
      addFooterText()
    }
  }

  handleAppointmentTimeChange = (e) => {
    let isWithin48Hours;
    if (e && e.start) {
      const meetingStart = e.start
      if (this.checkDateDiff(meetingStart) > 48) {
        isWithin48Hours = false
      } else {
        isWithin48Hours = true
      }
    }
    this.setState({ isWithin48Hours })
  }

  checkDateDiff = (date) => {
    return moment(date).diff(moment(new Date()), 'hours')
  }

  initializeApp = async () => {
    const organizer = await officeJsService.getOrganizer();
    const email = organizer.emailAddress
    const subscriptions = await bestelbewuster.listSubscriptions(email, true)

    // TODO: Do we use the subscription
    let subscription

    if (subscriptions.length === 0) {
      let outlookSubscription = await outlook.createSubscription()
      subscription = await bestelbewuster.createSubscription(email, outlookSubscription)
    } else {
      subscription = subscriptions[0]
    }

    let hash = await this.makeHash()
    // If i replace the event by hash it still crashes :)
    const event = await bestelbewuster.getEventByHash(hash)
    if (event) {
      this.setState({ event, meetingCreated: true })
      addFooterText()
    }

  }

  makeHash = async () => {
    let subject = await officeJsService.getSubject();
    let location = await officeJsService.getLocation();
    let organizer = await officeJsService.getOrganizer();
    let start = await officeJsService.getStartDate();
    let end = await officeJsService.getEndDate();

    let organizerFormat = organizer.emailAddress;
    let startDate = moment(start).utc().format();
    let endDate = moment(end).utc().format();

    return md5(subject + location + organizerFormat + startDate +endDate)
  }

  successToaster = () => {
    this.setState({ showSnack: true}, () => setTimeout(
      () => this.setState({
        showSnack: false,
        meetingCreated: true,
      }), 3000)
    )
  }

  renderAttendees = () => {
    const {
      event,
      openCollapse,
    } = this.state;
    return (
      <div className="event-container">
        <div className="padding">
          <DataField width="100%" label="Meeting id" content={event.id} />
        </div>
        <div className="divider" />
        {!event.attendees
            ? null
            : (
              <>
                <div className="padding">
                  <img src={UserIcon} className="icon inline-block" alt="users icon" />
                  &nbsp;Genodigden{event.attendees.length > 0 && ` (${event.attendees.length})`}
                </div>
                <div className="attendee-container">
                  {!event.attendees.length > 0 ? (
                    <div>Geen aanwezigen</div>
                  ) : (
                    event.attendees.map((att, i) => {
                      const active = openCollapse === i;
                      const order = att.orders && att.orders.length > 0;
                      const noLunch = att.status === "ACCEPTED" && !order
                      return (
                        <div key={`attendee-${i}`} className="attendee">
                          <div onClick={() => order && this.setState({ openCollapse: openCollapse !== undefined ? undefined : i})} className="bold">
                            <div className="attendee-email">
                              {att.email}
                              {
                                <a href={`https://demo.bestelbewuster.nl/#/event/${att.code}`}>
                                    <img
                                    className="edit-button"
                                    width="14"
                                    src={EditIcon}
                                    alt="status icon"
                                  />
                                </a>
                              }
                            </div>
                            {order && <img className={`plus-icon ${active ? 'plus-icon-active' : ''}`} src={PlusIcon} alt="plus icon" />}
                          </div>
                          <StatusIcon status={att.status} extraText={noLunch && '(geen lunch)'} />
                          {/* SHOW BOTH MEETING, MEETING USER & ORDER USER STATUS */}
                          <Collapse isOpened={active}>
                            {order && (
                              <div className="orders-container">
                                {att.orders.map((ord, idx) => (
                                  <React.Fragment key={idx}>
                                    <div className="bold">Bestelling {idx + 1}:</div>
                                    {ord.lines.map((x, index) => (
                                      <div key={index}>
                                        <div style={{width: '40px', display: 'inline-block', marginLeft: '6px'}}>• {x.quantity}x</div>
                                        <span className="italic">{x.product.caption}</span>
                                      </div>
                                    ))}
                                  </React.Fragment>
                                ))}
                              </div>
                            )}
                          </Collapse>
                        </div>
                      )
                    })
                  )}
                </div>
              </>
            )
          }
      </div>
    )
  }

  renderWithin48Hours = () => (
    <div className="text-box">
      <p>Het tijdstip van de meeting is binnen 48 uur.</p>
      <p>Het is helaas niet meer mogelijk om via BestelBewuster een meeting te plaatsen.</p>
      <p>Via onderstaande button kun je direct mail sturen naar jouw cateraar</p>
      <br />
      <a className="button" href="mailto:emailv@ndecateraar.nl?subject=Lunch bestelling">Bestel direct</a>
    </div>
  )

  renderAboutTomakeMeeting = () => (
    <div className="text-box">
      <p>Je staat op het punt een BestelBewuster lunchmeeting aan te maken.</p>
      <p>Wanneer je de afspraak verstuurt wordt deze aangemaakt en ontvangen de genodigden een email.</p>
    </div>
  )

  renderUnauthorized = () => (
    <div className="text-box">
       <div>Wil jij ook werken met de BestelBewuster plugin?</div>
       <br />
       <a
         href="mailto:info@bestelbewuster.nl?subject=Aanmelden BestelBewuster plugin"
         className="button"
       >
         Doe mee
       </a>
    </div>
  )

  render() {
    const {
      title,
      isOfficeInitialized,
    } = this.props;
    const {
      event,
      meetingCreated,
      isAuthorized,
      isWithin48Hours,
      showFooterText
    } = this.state;

    if (!isOfficeInitialized) {
      return (
        <Progress
          title={title}
          logo='assets/logo-filled.png'
          message='Please sideload your addin to see app body.'
        />
      )
    }

    return (
      <div className="content-container">
        <Header logo='assets/logo-filled.png' title={this.props.title} message='BestelBewuster' />

             {isAuthorized
               ? isWithin48Hours
                  ? this.renderWithin48Hours()
                  : meetingCreated
                     ? event && this.renderAttendees()
                    : this.renderAboutTomakeMeeting()
               : this.renderUnauthorized()
             }

             {meetingCreated
               ? <div className="padding">
                    <input
                      className="inline-block"
                      type="checkbox"
                      checked={showFooterText}
                      onChange={() => this.setState({ showFooterText: !showFooterText })}
                    />
                    <label>Toon BestelBewuster footer</label>
                 </div>
               : null
             }

             {meetingCreated
              ? <div
                  onClick={() => console.log('@Niels: Moet op de backend worden aangesloten')}
                  className="button button--cancel margin"
                >
                  Annuleer deze meeting
                </div>
              : null
             }
             <div className="padding small">Hulp nodig? Wij <a href={`mailto:help@bestelbewuster.nl?subject=Vraag of hulp nodig bij ${event ? ` meeting ${event.id}` : 'het aanmaken van een meeting'} `}>helpen</a> je graag.</div>
      </div>
    );
  }
}
