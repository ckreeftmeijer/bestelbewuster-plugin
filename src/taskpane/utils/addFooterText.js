import React from 'react'
import OfficeJSService from '../services/OfficeJS'
const officeJsService = new OfficeJSService()

const addFooterText = async () => {
  const item = Office.context.mailbox.item;
  if (!item) return

  const body = await officeJsService.getBody()
  if (body.includes('Deze meeting betreft een lunchmeeting')) {
    return
  }

  const signature = (
      <div>
        --<br />
        <div>Deze meeting betreft een lunchmeeting. Hiervoor werken wij samen met BestelBewuster. Bij aanwezigheid krijg je via hun bericht om zelf je lunch aan te geven. Je kunt ook zelf de BestelBewuster invoegtoepassing installeren om een samenvatting van jouw bestelling in Outlook te zien.</div>
      </div>
  )

  const htmlBody = body.replace('</body>', `${signature}<br /></body>`)

  const textBody = `${body}

--
Deze meeting betreft een lunchmeeting. Hiervoor werken wij samen met BestelBewuster. Bij aanwezigheid krijg je via hun bericht om zelf je lunch aan te geven. Je kunt ook zelf de BestelBewuster invoegtoepassing installeren om een samenvatting van jouw bestelling in Outlook te zien.
`

  item.body.getTypeAsync(
      function (result) {
          if (result.status === Office.AsyncResultStatus.Failed){
              console.log(result.error.message);
          }
          else {
              // Successfully got the type of item body.
              // Set data of the appropriate type in body.
              if (result.value === Office.MailboxEnums.BodyType.Html) {
                  // Body is of HTML type.
                  // Specify HTML in the coercionType parameter
                  // of setSelectedDataAsync.
                  item.body.setAsync(
                      htmlBody,
                      { coercionType: Office.CoercionType.Html,
                      asyncContext: { var3: 1, var4: 2 } },
                      function (asyncResult) {
                          if (asyncResult.status ===
                              Office.AsyncResultStatus.Failed){
                              console.log(asyncResult.error.message);
                          }
                          else {
                              // Successfully set data in item body.
                              // Do whatever appropriate for your scenario,
                              // using the arguments var3 and var4 as applicable.
                          }
                      });
              }
              else {
                  // Body is of text type.
                  item.body.setAsync(
                    textBody,
                      { coercionType: Office.CoercionType.Text,
                          asyncContext: { var3: 1, var4: 2 } },
                      function (asyncResult) {
                          if (asyncResult.status ===
                              Office.AsyncResultStatus.Failed){
                              console.log(asyncResult.error.message);
                          }
                          else {
                              // Successfully set data in item body.
                              // Do whatever appropriate for your scenario,
                              // using the arguments var3 and var4 as applicable.
                          }
                       });
              }
          }
      });


}

export default addFooterText
