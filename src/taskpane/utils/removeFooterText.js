import OfficeJSService from '../services/OfficeJS'
const officeJsService = new OfficeJSService()

const removeFooterText = async () => {
  const item = Office.context.mailbox.item;
  if (!item) return

  const body = await officeJsService.getBody()
  const textSignature = 'Deze meeting betreft een lunchmeeting. Hiervoor werken wij samen met BestelBewuster. Bij aanwezigheid krijg je via hun bericht om zelf je lunch aan te geven. Je kunt ook zelf de BestelBewuster invoegtoepassing installeren om een samenvatting van jouw bestelling in Outlook te zien.'

  const footerIndex = body.indexOf(textSignature)
  const strippedBodybody = body.slice(0, footerIndex - 5)

  item.body.getTypeAsync(
      function (result) {
          if (result.status === Office.AsyncResultStatus.Failed){
              console.log(result.error.message);
          }
          else {
              // Successfully got the type of item body.
              // Set data of the appropriate type in body.
              if (result.value === Office.MailboxEnums.BodyType.Html) {
                  // Body is of HTML type.
                  // Specify HTML in the coercionType parameter
                  // of setSelectedDataAsync.
                  item.body.setAsync(
                      strippedBodybody,
                      { coercionType: Office.CoercionType.Html,
                      asyncContext: { var3: 1, var4: 2 } },
                      function (asyncResult) {
                          if (asyncResult.status ===
                              Office.AsyncResultStatus.Failed){
                              console.log(asyncResult.error.message);
                          }
                          else {
                              // Successfully set data in item body.
                              // Do whatever appropriate for your scenario,
                              // using the arguments var3 and var4 as applicable.
                          }
                      });
              }
              else {
                  // Body is of text type.
                  item.body.setAsync(
                    strippedBodybody,
                      { coercionType: Office.CoercionType.Text,
                          asyncContext: { var3: 1, var4: 2 } },
                      function (asyncResult) {
                          if (asyncResult.status ===
                              Office.AsyncResultStatus.Failed){
                              console.log(asyncResult.error.message);
                          }
                          else {
                              // Successfully set data in item body.
                              // Do whatever appropriate for your scenario,
                              // using the arguments var3 and var4 as applicable.
                          }
                       });
              }
          }
      });


}

export default removeFooterText
