class OfficeService {

  getSubject = () =>
    {
        return new Promise((resolve, reject) => {
            Office.context.mailbox.item.subject.getAsync((result) => {
                resolve(result.value)
            })
        })
    }

  getLocation = () =>
    {
        return new Promise((resolve, reject) => {
            Office.context.mailbox.item.location.getAsync((result) => {
                resolve(result.value)
            })
        })
    }

getOrganizer = () =>
    {
        return new Promise((resolve, reject) => {
            Office.context.mailbox.item.organizer.getAsync((result) => {
                resolve(result.value)
            })
        })
    }

getStartDate = () =>
    {
        return new Promise((resolve, reject) => {
            Office.context.mailbox.item.start.getAsync((result) => {
                resolve(result.value)
            })
        })
    }

getEndDate = () =>
    {
        return new Promise((resolve, reject) => {
            Office.context.mailbox.item.end.getAsync((result) => {
                resolve(result.value)
            })
        })
    }

getRestKey = () =>
    {
        return new Promise((resolve, reject) => {
            Office.context.mailbox.getCallbackTokenAsync({
                isRest: true
            }, (result) => {
                resolve(result.value)
            });
        })
    }

getBodyType = () =>
    {
        return new Promise((resolve, reject) => {
            Office.context.mailbox.item.body.getTypeAsync((result) => {
                resolve(result.value)
            })
        })
    }

getBody = () =>
    {
        return new Promise((resolve, reject) => {
            Office.context.mailbox.item.body.getAsync('text', null, (result) => {
              // Office.CoercionType.Html
                resolve(result.value)
            })
        })
    }
}

export default OfficeService;
