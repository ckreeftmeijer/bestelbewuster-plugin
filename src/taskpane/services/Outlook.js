import { outlookApiUrl, subscription } from '../../config'
import OfficeJSService from './OfficeJS'

const officeJsService = new OfficeJSService()

class OfficeService {

  createSubscription = async () => {
    return this.sendRequest(
      'me/subscriptions',
      'post',
        {
          body: JSON.stringify(subscription.payload)
        }
    )
  }

  sendRequest = async (url, method, options) => {
    const key = await officeJsService.getRestKey()

    return fetch(`${outlookApiUrl}${url}`, {
      method,
      headers: {
        "Content-type": "application/json",
        "Authorization": `Bearer ${key}`
      },
      ...options
    })
    .then(resp => resp.json())
    .then(data => data)
    .catch(error => error);
  }
}

export default OfficeService;
