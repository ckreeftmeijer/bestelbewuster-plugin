import { apiUrl } from '../../config'

class BestelBewusterService {

  listSubscriptions = (owner, active) => {
    return this.sendRequest(`subscriptions?owner=${owner}&active=${active}`)
  }

  createSubscription = (owner, outlookSubscription) => {
    return this.sendRequest('subscriptions',
      'post',
      {
        body: JSON.stringify(
          {
            code: outlookSubscription.Id,
            owner,
            expires_at: outlookSubscription.SubscriptionExpirationDateTime
          }
        )
      }
    )
  }

  getEventByHash = (hash) => {
      return this.sendRequest(`events/hash/${hash}`)
   }

  sendRequest = (url, method, options) => {
    return fetch(`${apiUrl}${url}`, {
      method,
      headers: {
        "Content-type": "application/json"
      },
      ...options
    })
    .then(resp => resp.json())
    .then(data => data)
    .catch(error => error);
  }
}

export default BestelBewusterService;
